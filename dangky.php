<?php
session_start();

function isRequired($value)
{
    return !empty($value) && trim($value) !== "";
}

function checkFormatDate($format, $value)
{
    $date = DateTime::createFromFormat($format, $value);
    return boolval($date) && $date->format($format) === $value;
}

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $errors = [];
    $validator = [
        "name" => function ($value) {
            if (!isRequired($value)) {
                return "Hãy nhập tên";
            }
        },
        "gender" => function ($value) {
            if (!isRequired($value)) {
                return "Hãy nhập giới tính";
            }
        },
        "department" => function ($value) {
            if (!isRequired($value) || $value === "-1") {

                return "Hãy nhập phân khoa";
            }
        },
        "birthday" => function ($value) {

            if (!isRequired($value)) {
                return "Hãy nhập ngày sinh";
            }
            if (!checkFormatDate("d/m/Y", $value)) {
                return "Hãy nhập ngày sinh đúng định dạng";
            }
        }
    ];

    foreach ($validator as $key => $item) {
        $val = isset($_POST[$key]) ? $_POST[$key] : null;
        $result = $item($val);
        if (!is_null($result)) {
            $errors[$key] = $result;
        }
    }

    $types = [
        'image/png' => 'png',
        'image/jpeg' => 'jpg'
    ];

    $filepath = $_FILES['file']['tmp_name'];
    $fileinfo = finfo_open(FILEINFO_MIME_TYPE);
    $filetype = finfo_file($fileinfo, $filepath);
    if(!in_array($filetype, array_keys($types))) {
       $errors['file'] = 'Định dạng file chưa đúng';
    }

    if (count($errors) == 0) {
        $data = [];

        foreach($validator as $key => $item) {
            $data[$key] = $_POST[$key];
        }
        if (isset($_FILES['file'])){
            $file = $_FILES['file'];
            $content = file_get_contents($file['tmp_name']);
            $base64 = $src = 'data: '.mime_content_type($file['tmp_name']).';base64,'.base64_encode($content);
            $data['img'] = $base64;

            $dir = "./upload/";
            mkdir($dir,0700);
            $nameImg = explode(".", $_FILES["file"]["name"]);
            $name = $nameImg[0]."_".date("YmdHis").".".$nameImg[1];
            $target_file = $dir . basename($name);
            move_uploaded_file($filepath, $target_file);

        } else {
            $data['img'] = null;
        }

        if (isset($_POST['address'])) {
            $data['address'] = $_POST['address'];
        } else {
            $data['address'] = null;
        }

        $_SESSION['data'] = $data;

        header('Location: confirm.php');
    }


}

$gioiTinh = [
    "0" => "Nam",
    "1" => "Nữ"
];

$khoa = [
    "-1" => "Trống",
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css'
          media="screen"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
          type="text/css"/>
    <style>
        .container {
            display: flex;
            height: 100vh;
            justify-content: center;
        }

        .wrapper {
            display: flex;
            justify-content: center;
            flex-direction: column;
        }

        form {
            width: 500px;
            border: 1px solid rgb(188, 208, 246);
            padding: 30px;
        }

        p {
            background: lightgray;
            padding: 8px;
        }

        .name {
            margin: 12px 0px;
            display: flex;
        }

        .gtinh {
            margin-top: 12px;

        }

        .radio,
        .khoa {
            margin-left: 20px;
            border: 1px solid rgb(87, 137, 245);
        }

        .lab {
            background: cornflowerblue;
            padding: 8px 10px;
            border: 2px solid blue;
            width: 25%;
            display: block;
            color: white;
        }

        .inp {
            margin-left: 20px;
            width: 60%;
            border: 1px solid rgb(87, 137, 245);
        }

        button {
            background: rgb(4, 129, 44);
            padding: 8px 10px;
            border: solid 2px rgb(154, 154, 232);
            width: 25%;
            display: block;
            color: white;
            border-radius: 6px;
        }

        .submit {
            display: flex;
            justify-content: center;
        }

        .gioitinh-wrapper > div {
            margin-left: 20px;
            margin-top: 8px;
        }

        #date {
            margin-left: 20px;
        }

        .required {
            color: red;
            font-weight: bold;
            margin-left: 3px;
        }

        .errors > div {
            color: red;
            font-weight: 600;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="wrapper">

        <div>


            <form action="dangky.php" enctype="multipart/form-data" method="post">

                <div class="errors">
                    <?php if (isset($errors) && is_array($errors) && count($errors) > 0) { ?>
                        <?php
                        foreach ($errors as $item) {
                            ?>
                            <div><?= $item ?></div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="name">
                    <label class="lab">Họ và tên <span class="required">*</span></label>
                    <input class="inp" type="text" name="name"/>
                </div>
                <div class="name gioitinh-wrapper">
                    <label class="lab">Giới tính <span class="required">*</span></label>
                    <div>
                        <?php
                        foreach ($gioiTinh as $key => $item) {
                            ?>
                            <label for="<?= $key ?>" class="gtinh"><?= $item ?></label>
                            <input type="radio" name="gender" value="<?= $item ?>">
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="name">
                    <label class="lab">Phân Khoa <span class="required">*</span></label>
                    <select name="department" id="khoa" class="khoa">
                        <?php
                        foreach ($khoa as $key => $item) {
                            ?>
                            <option value="<?= $key ?>"><?= $item ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="name">
                    <label class="lab">Ngày sinh <span class="required">*</span></label>
                    <input id="date" name="birthday" type="text" class="ip date" name="date" placeholder="dd/mm/yyyy"/>
                </div>
                <div class="name">
                    <label class="lab">Địa chỉ</label>
                    <input name="address" class="inp" type="input">
                </div>
                <div class="name">
                    <label class="lab" >Hình ảnh</label>
                    <input style="margin-left: 20px" type="file" name="file">
                </div>
                <div class="submit">
                    <button>Đăng kí</button>
                </div>
            </form>
        </div>

    </div>

</div>
</div>
</body>
<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript"
        src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
<script>
    $('#date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'dd/mm/yyyy',
    });
</script>

</html>
